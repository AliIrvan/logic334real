-- QUIZ 2
-- Membuat Database
CREATE DATABASE DB_Entertrainer

-- Membuat Table
CREATE TABLE artis (
	artis_ID BIGINT PRIMARY KEY IDENTITY(1,1),
	kd_artis VARCHAR(100) NOT NULL,
	nm_artis VARCHAR(100) NOT NULL,
	jk VARCHAR(100) NOT NULL,
	bayaran BIGINT NOT NULL,
	award INT NOT NULL,
	negara VARCHAR(100) NOT NULL
)

CREATE TABLE film(
	film_ID BIGINT PRIMARY KEY IDENTITY(1,1),
	kd_film VARCHAR(10) NOT NULL,
	nm_film VARCHAR(55) NOT NULL,
	genre VARCHAR(55) NOT NULL,
	artis VARCHAR(55) NOT NULL,
	produser VARCHAR(55) NOT NULL,
	pendapatan BIGINT NOT NULL,
	nominasi INT NOT NULL
)

CREATE TABLE produser(
	produser_ID BIGINT PRIMARY KEY IDENTITY(1,1),
	kd_produser VARCHAR(50) NOT NULL,
	nm_produser VARCHAR(50) NOT NULL,
	international VARCHAR(50) NOT NULL
)

CREATE TABLE negara(
	negara_ID BIGINT PRIMARY KEY IDENTITY(1,1),
	kd_negara VARCHAR(100) NOT NULL,
	nm_negara VARCHAR(100) NOT NULL
)

CREATE TABLE genre(
	genre_ID BIGINT PRIMARY KEY IDENTITY(1,1),
	kd_genre VARCHAR(50) NOT NULL,
	nm_genre VARCHAR(50) NOT NULL,
)

-- Mengisi data table
INSERT INTO
	artis (kd_artis, nm_artis, jk, bayaran, award, negara)
VALUES
	('A001','ROBERT DOWNEY JR','PRIA',3000000000,2,'AS'),
	('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
	('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
	('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
	('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')

INSERT INTO
	film (kd_film, nm_film, genre, artis, produser, pendapatan, nominasi)
VALUES
	('F001','IRON MAN','G001','A001','PD01',2000000000,3),
	('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
	('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
	('F004','AVENGER : CIVIL WAR','G001','A001','PD01',2000000000,1),
	('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
	('F006','THE RAID','G001','A004','PD03',800000000,5),
	('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
	('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
	('F009','POLICE STORY','G001','A003','PD02',700000000,3),
	('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
	('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
	('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
	('F013','KUNGFU PANDA','G003','A003','PD05',923000000,5)

INSERT INTO
	produser (kd_produser, nm_produser, international)
VALUES
	('PD01','MARVEL','YA'),
	('PD02','HONGKONG CINEMA','YA'),
	('PD03','RAPI FILM','TIDAK'),
	('PD04','PARKIT','TIDAK'),
	('PD05','PARAMOUNT PICTURE','YA')

INSERT INTO 
	negara (kd_negara, nm_negara)
VALUES
	('AS','AMERIKA SERIKAT'),
	('HK','HONGKONG'),
	('ID','INDONESIA'),
	('IN','INDIA')

INSERT INTO
	genre (kd_genre, nm_genre)
VALUES
	('G001','ACTION'),
	('G002','HORROR'),
	('G003','COMEDY'),
	('G004','DRAMA'),
	('G005','THRILLER'),
	('G006','FICTION')


-- 1. Tampilkan jumlah pendapatan produser MARVEL secara keseluruhan
SELECT
	produser.nm_produser,
	SUM(film.pendapatan) AS totalPendapatan
FROM
	produser
INNER JOIN
	film
ON
	produser.kd_produser = film.produser
--WHERE
--	produser.nm_produser = 'MARVEL'
GROUP BY
	produser.nm_produser
HAVING
	produser.nm_produser = 'MARVEL'

-- 2. Tampilkan nama film dan nominasi yang tidak mendapatkan nominasi
SELECT
	film.nm_film,
	film.nominasi
FROM
	film
WHERE
	film.nominasi = 0
	
-- 3. Tampilkan nama film yang huruf depannya 'p'
SELECT
	film.nm_film
FROM
	film
WHERE
	nm_film LIKE 'P%'
-- WHERE SUBSTRING(nm_film,1,1) = 'P'

-- 4. Tampilkan nama film yang huruf terakhir 'y'
SELECT
	film.nm_film
FROM
	film
WHERE
	nm_film LIKE '%y'
-- WHERE SUBSTRING(nm_film, DATALENGTH(nm_film),1) = 'Y'

-- 5. Tampilkan nama film yang mengandung huruf 'd'
SELECT
	film.nm_film
FROM
	film
WHERE
	nm_film LIKE '%d%'

-- 6. Tampilkan nama film dan artis
SELECT
	film.nm_film,
	artis.nm_artis
FROM
	film
INNER JOIN
	artis
ON
	film.artis = artis.kd_artis
WHERE
	nm_film LIKE '%IRON%' OR nm_artis LIKE '%JACKIE%'

-- 7. Tampilkan nama film yg artis berasal dari hongkong
SELECT
	film.nm_film,
	artis.nm_artis,
	negara.nm_negara
FROM
	film
INNER JOIN
	artis
ON
	film.artis = artis.kd_artis
INNER JOIN
	negara
ON
	artis.negara = negara.kd_negara
WHERE
	NOT nm_negara IN ('HONGKONG','INDONESIA')

-- 8. Tampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf O
SELECT
	film.nm_film, 
	negara.nm_negara
FROM
	film
INNER JOIN
	artis
ON
	film.artis = artis.kd_artis
INNER JOIN
	negara
ON
	artis.negara = negara.kd_negara
WHERE
	negara.nm_negara NOT LIKE '%O%'

-- 9. Tampilkan nama artis yang tidak pernah bermain film
SELECT
	artis.nm_artis
FROM
	artis
LEFT JOIN
	film
ON
	artis.kd_artis = film.artis
WHERE
	artis IS NULL

-- 10. Tampilkan nama artis yang bermain film dengan genre drama
SELECT
	artis.nm_artis,
	genre.nm_genre
FROM
	artis
INNER JOIN
	film
ON
	artis.kd_artis = film.artis
INNER JOIN
	genre
ON
	film.genre = genre.kd_genre
WHERE
	nm_genre = 'DRAMA'

-- 11. Tampilkan nama artis yang bermain film dengan genre action
SELECT
	DISTINCT(artis.nm_artis),
	genre.nm_genre
FROM
	artis
INNER JOIN
	film
ON
	artis.kd_artis = film.artis
INNER JOIN
	genre
ON
	film.genre = genre.kd_genre
WHERE
	nm_genre = 'ACTION'

-- 12. Tampilkan data negara dengan jumlah filmnya
SELECT
	negara.kd_negara,
	negara.nm_negara,
	COUNT(film.artis) AS jumlahFIlm
FROM
	negara
LEFT JOIN
	artis
ON
	negara.kd_negara = artis.negara
LEFT JOIN
	film
ON
	artis.kd_artis = film.artis
GROUP BY
	kd_negara,nm_negara

-- 13. Menampilkan nama film yang skala international
SELECT
	film.nm_film
FROM
	film
INNER JOIN
	produser
ON
	film.produser = produser.kd_produser
WHERE
	produser.international = 'YA'

--14. menampilkan jumlah film dari masing masing produser
SELECT
	produser.nm_produser,
	COUNT(film.produser) AS jumlahFilm
FROM
	produser
LEFT JOIN
	film
ON
	produser.kd_produser = film.produser
GROUP BY
	produser.nm_produser
HAVING
	nm_produser = 'MARVEL' AND  COUNT(film.produser)> 3
