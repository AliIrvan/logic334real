-- QUIZ SQL 03

CREATE DATABASE DB_Sales

CREATE TABLE SALESPERSON(
	ID BIGINT PRIMARY KEY IDENTITY(1,1),
	name VARCHAR(50) NOT NULL,
	bod DATE NOT NULL,
	salary DECIMAL(18,2)
)


CREATE TABLE ORDERS(
	ID BIGINT PRIMARY KEY IDENTITY(1,1),
	order_date DATE NOT NULL,
	cust_id BIGINT NOT NULL,
	salesperson_id BIGINT NOT NULL,
	amount DECIMAL(18,2) NOT NULL
)

INSERT INTO SALESPERSON(name, bod, salary)
VALUES
	('Abe','9/11/1988',140000),
	('Bob','9/11/1978',44000),
	('Chris','9/11/1983',40000),
	('Dan','9/11/1980',52000),
	('Ken','9/11/1977',115000),
	('Joe','9/11/1990',38000)
	
INSERT INTO ORDERS(order_date, cust_id, salesperson_id, amount)
VALUES
	('8/2/2020', 4, 2, 540),
	('1/22/2021', 4, 5, 1800),
	('7/14/2019', 9, 1, 460),
	('1/29/2018', 7 , 2,2400),
	('2/3/2021', 6, 4, 600),
	('3/2/2020', 6, 4, 720),
	('5/6/2021', 9, 4, 150)

-- 1. Informasi nama sales yang memiliki order lebih dari 1
SELECT
	SP.name, 
	(COUNT(O.SALESPERSON_ID)) AS totalORDER
FROM
	SALESPERSON AS SP
INNER JOIN
	ORDERS AS O
ON
	SP.ID = O.salesperson_id
GROUP BY
	SP.name
HAVING
	COUNT(O.SALESPERSON_ID) > 1
	

-- 2. Informasi nama sales yang total amount ordernya diatas 1000
SELECT
	SP.name,
	SUM(O.amount) AS totalAmount
FROM
	SALESPERSON AS SP
INNER JOIN
	ORDERS AS O
ON
	SP.ID = O.salesperson_id
GROUP BY
	SP.name
HAVING
	SUM(O.amount) > 1000

-- 3. Informasi nama sales, umur, gaji, dan total amount order yang tahun ordernya >= 2020, tampilkan ASC sesuai umur
SELECT
	SP.name,
	--DATEDIFF(YEAR, SP.bod, GETDATE()) AS umur,
	FLOOR(DATEDIFF(DAY, SP.bod, GETDATE()) / 365.25) AS umur,
	SP.salary,
	SUM(O.amount) AS totalAmount
FROM
	SALESPERSON AS SP
INNER JOIN
	ORDERS AS O
ON
	SP.ID = O.salesperson_id
WHERE
	YEAR(order_date) >= 2020 -- RETURN INT
GROUP BY
	SP.name, SP.bod, SP.salary
ORDER BY
	umur ASC

-- 4. Carilah rata rata total amount masing masing sales urutkan dari hasil yang besar
SELECT
	SP.name,
	CAST(AVG(O.amount) AS DECIMAL (18,2)) AS rataRata
FROM 
	SALESPERSON AS SP
LEFT JOIN 
	ORDERS AS O
ON 
	SP.ID = O.salesperson_id 
GROUP BY
	SP.name
ORDER BY
	rataRata DESC

-- 5. Perusahaan akan memberikan bonus bagi sales yang berhasil memiliki order dan total order labih dari 1000 sebanyak 30% dari salarynya
SELECT
	SP.name,
	COUNT(O.salesperson_id) AS totalOrder,
	SUM(O.amount) AS totalAmount,
	CAST((SP.salary * 30 / 100) AS DECIMAL(10,2)) AS Bonus
FROM 
	SALESPERSON AS SP
LEFT JOIN
	ORDERS AS O
ON
	SP.ID = O.salesperson_id
GROUP BY
	SP.name, SP.salary
HAVING
	COUNT(O.salesperson_id) > 2 AND SUM(O.amount) > 1000


-- 6. Tampilkan data sales yang belum memiliki orderan sama sekali
SELECT
	SP.name,
	COUNT(O.salesperson_id) AS totalOrder
FROM
	SALESPERSON AS SP
LEFT JOIN
	ORDERS AS O
ON
	SP.ID = O.salesperson_id
WHERE
	O.salesperson_id IS NULL 
GROUP BY
	SP.name

-- 7. gaji sales akan dipotong jika tidak memiliki orderan sebesar 2%
SELECT
	SP.name,
	SP.salary AS gajiAwal,
	CAST((SP.salary - (SP.salary * 2 /100)) AS DECIMAL(18,2)) AS gaji
FROM
	SALESPERSON AS SP
LEFT JOIN
	ORDERS AS O
ON
	SP.ID = O.salesperson_id
WHERE
	O.salesperson_id IS NULL 

