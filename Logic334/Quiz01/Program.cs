﻿//tugas1();
//tugas2();
//tugas3();
tugas4();
//tugas5();
//tugas6();


Console.ReadKey();

static void tugas1()
{
    Console.WriteLine("== PROGRAM MENCARI LUAS DAN KELILING LINGKARAN ==\n");

    // INPUT
    Console.Write("Masukkan jari jari lingkaran (r) : ");
    double r = double.Parse(Console.ReadLine());
    Console.WriteLine();

    //OUTPUT
    double luas = Math.PI * r * r;
    Console.WriteLine($" Luas Lingkaran = {luas}");
    double keliling = 2 * Math.PI * r;
    Console.WriteLine($" Keliling Lingkaran = {Math.Round(keliling, 2, MidpointRounding.ToEven)}");

    Console.WriteLine();
}

static void tugas2()
{
    Console.WriteLine("== PROGRAM MENCARI LUAS DAN KELILING PERSEGI ==\n");

    // INPUT
    Console.Write("Masukkan nilai sisi (s) : ");
    int s = int.Parse(Console.ReadLine());
    Console.WriteLine();

    // OUTPUT
    int luasPersegi = s * s;
    Console.WriteLine($" Luas Persegi = {luasPersegi}");
    int kelilingPersegi = 4 * s;
    Console.WriteLine($" Keliling Persegi = {kelilingPersegi}");

    Console.WriteLine();
}

static void tugas3()
{
    Console.WriteLine("== PROGRAM MODULUS ==\n");

    // INPUT
    Console.Write("Masukkan angka : ");
    int angka = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Pembagi : ");
    int pembagi = int.Parse(Console.ReadLine());

    Console.WriteLine();

    // OUTPUT
    int hasilModulus = angka % pembagi;
    if (hasilModulus == 0)
    {
        Console.WriteLine($"{angka} % {pembagi} adalah 0");
    }
    else
    {
        Console.WriteLine($"{angka} % {pembagi} bukan nol melainkan {hasilModulus}");
    }
    Console.WriteLine();
}

static void tugas4()
{
    Console.Write("Masukkan nilai puntung : ");
    int puntung = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai batang : ");
    int batang = int.Parse(Console.ReadLine());
    Console.WriteLine();

    Console.WriteLine($"{puntung} Puntung rokok dirangkai menjadi {batang} batang rokok \n");

    Console.WriteLine("Berapa jumlah batang yang didapatkan");
    Console.Write("Masukkan jumlah puntung rokok yang terkumpul : ");
    int puntungTerkumpul = int.Parse(Console.ReadLine());

    int batangTerangkai = Convert.ToInt32(puntungTerkumpul / puntung);
    int sisa = puntungTerkumpul % puntung;
    Console.WriteLine($"{batangTerangkai} buah batang rokok dan sisa {sisa} buah puntung rokok");
    Console.WriteLine();

    Console.WriteLine("Berapakan penghasilan yang didapatkan ?");
    Console.Write("masukkan harga perbatang : ");
    int harga = int.Parse(Console.ReadLine());
    int penghasilan = batangTerangkai * harga;
    Console.WriteLine($"Jumlah penghasilan yang didapatkan = {penghasilan} rupiah");
    Console.WriteLine();
}

static void tugas5()
{
    Console.WriteLine("== PROGRAM PENILAIAN==\n");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    if (nilai >= 80 && nilai <= 100)
    {
        Console.WriteLine("Grade = A ");
    }
    else if (nilai >= 60 && nilai < 80)
    {
        Console.WriteLine("Grade = B ");
    }
    else if (nilai >= 0 && nilai < 60)
    {
        Console.WriteLine("Grade = C ");
    }
    else
    {
        Console.WriteLine("Nilai tidak valid");
    }
}   

static void tugas6()
{
    Console.WriteLine("== PROGRAM CEK ANGKA GANJIL ATAU GENAP ==\n");
    Console.Write("Masukkan angka : ");
    int angka = int.Parse(Console.ReadLine());

    if (angka % 2 == 0)
    {
        Console.WriteLine($"Angka {angka} adalah bilangan genap");
    }
    else
    {
        Console.WriteLine($"Angka {angka} adalah bilangan ganjil");
    }

    Console.WriteLine("\nCara Ternary");
    string tipe = (angka % 2) == 0 ? $"Angka {angka} adalah bilangan genap" : $"Angka {angka} adalah bilangan ganjil";
    Console.WriteLine(tipe);
}