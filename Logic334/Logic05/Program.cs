﻿//padLeft();
rekursifFunction();
Console.ReadKey();

static void rekursifFunction()
{
    Console.WriteLine("== Rekursif Function ==\n");

    Console.Write("Masukkan input awal : ");
    int start = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Input akhir : ");
    int end = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Type (ASC/DESC) : ");
    string type = Console.ReadLine().ToUpper();

    // Panggil Fungsi 
    perulangan(start, end, type);
}

static int perulangan(int start, int end, string type)
{
    if (type == "DESC")
    {
        if (start == end)
        {
            Console.WriteLine(end);
            return 0;
        }
        Console.WriteLine(end);
        return perulangan(start, end - 1, type);
    } else
    {
        if (start == end)
        {
            Console.WriteLine(start);
            return 0;
        }
        Console.WriteLine(start);
        return perulangan(start + 1, end, type);
    }
}
static void padLeft()
{
    Console.WriteLine("== PadLeft ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Panjang Karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Char = ");
    char chars = char.Parse(Console.ReadLine());

    //OUTPUT : 231100001 -> 2 digit tahun, 2 digit bulan + generate Length

    DateTime data = DateTime.Now;

    string code = "";
    string code1 = "";

    code = data.ToString("yyMM") + input.ToString().PadLeft(panjang, chars);
    //code1 = data.ToString("yyMM") + input.ToString().PadRight(panjang, chars);

    Console.WriteLine($"Hasil Padleft : {code}");
}