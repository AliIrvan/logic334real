﻿//test1();
//test2();
//test3();
test3b();

static void test1()
{
    Console.Write("masukkan teks input = ");
    string input = Console.ReadLine();

    //char[] teksInput = input.ToCharArray();
    int count = 0;

    foreach (char t in input)
    {
        if (char.IsUpper(t))
        {
            count++;
        }
    }

    Console.WriteLine($"Output = {count}");
}

static void test2()
{
    Console.Write(" masukkan input Start = ");
    int start = int.Parse(Console.ReadLine());
    Console.Write(" masukkan input End = ");
    int end = int.Parse(Console.ReadLine());

    string inisialInstansi = "XA";
    DateTime tgl = DateTime.Now;

    Console.WriteLine();
    for (int i = start; i <= end; i++)
    {
        Console.WriteLine($"{inisialInstansi}-{tgl.ToString("ddMMyyyy")}-{i.ToString().PadLeft(5, '0')}");
    }

}

static void test3()
{
    Console.Write("masukkan input = ");
    string[] inputOutput = Console.ReadLine().Split(",");

    string input = inputOutput[0];
    int loop = int.Parse(inputOutput[1]);

    char[] angkaInput = inputOutput[0].ToArray();
    int[] inputInt = Array.ConvertAll(angkaInput, c => (int)char.GetNumericValue(c));

    int hasilPenjumlahan = inputInt.Sum() * loop;
    Console.WriteLine($"Hasil Penjumlahan = {hasilPenjumlahan}");
    char[] output = hasilPenjumlahan.ToString().ToCharArray();

    int[] digitOutput = Array.ConvertAll(output, c => (int)char.GetNumericValue(c));
    int penjumlahanDigit = digitOutput.Sum();
    Console.WriteLine($"Output = {penjumlahanDigit}");
}

static void test3b()
{
    Console.Write("masukkan input = ");
    string[] inputOutput = Console.ReadLine().Split(",");

    string input = inputOutput[0];
    int loop = int.Parse(inputOutput[1]);

    char[] angkaInput = inputOutput[0].ToArray();
    int[] inputInt = Array.ConvertAll(angkaInput, c => (int)char.GetNumericValue(c));

    int hasil = inputInt.Sum() * loop;
    perulangan(hasil);
   
}

static int perulangan(int hasil)
{
    int x = 0;
    if (hasil.ToString().Length == 1)
    {
        Console.WriteLine(hasil);
        return 0;
    }
    else
    {
        string STRhasil = hasil.ToString();
        foreach(char c in STRhasil)
        {
            x += int.Parse(c.ToString());
        }
        hasil = x;
        return perulangan(hasil);
    }
}
