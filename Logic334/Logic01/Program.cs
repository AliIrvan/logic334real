﻿//konversi();
//operatorAritmatika();
//operatorModulus();
//operatorPenugasan();
//operatorPerbandingan();
//operatorLogika();
MethodReturnType();

Console.ReadKey();

static void MethodReturnType() {
    Console.WriteLine("== Cetak Method return type==\n");
    Console.Write("Masukkan Mangga : ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    int apel = int.Parse(Console.ReadLine());

    int jumlah = hasil(mangga, apel);
    Console.WriteLine($"Jumlah mangga + apel = {jumlah}");
};
static int hasil(int mangga, int apel) {
    int hasil;
    hasil = mangga + apel;
    return hasil;
};
static void operatorLogika() {
    Console.WriteLine("== Operator Logika ==\n");
    Console.Write("Enter your age : ");
    int age = int.Parse(Console.ReadLine());
    Console.Write("Enter your password : ");
    string password = Console.ReadLine();

    bool isAdult = age > 18;
    bool isAuthorize = password == "admin";

    if (isAdult && isAuthorize)
    {
        Console.WriteLine("Welcome to the club");
    }
    else {
        Console.WriteLine("Sorry, Try Again");
    }
};
static void operatorPerbandingan() {

    int mangga, apel;
    Console.WriteLine("== Operator Perbandingan ==\n");
    Console.Write("Masukkan Mangga = ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel = ");
    apel = int.Parse(Console.ReadLine());
    Console.WriteLine();

    Console.WriteLine("Hasil Perbandingan : ");
    Console.WriteLine($"Mangga > Apel : {mangga > apel}");
    Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
    Console.WriteLine($"Mangga < Apel : {mangga < apel}");
    Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
    Console.WriteLine($"Mangga == Apel : {mangga == apel}");
    Console.WriteLine($"Mangga != Apel : {mangga != apel}");
};

static void operatorPenugasan() {

    int mangga, tambahan;
    Console.WriteLine("== Operator Penugasan ==\n");
    // INPUT
    Console.Write("Masukkan Jumlah Mangga = ");
    mangga = int.Parse(Console.ReadLine());
    Console.WriteLine($"Jumlah mangga = {mangga} \n");
    Console.Write("Masukkan Jumlah tambahan = ");
    tambahan = int.Parse(Console.ReadLine());
    Console.WriteLine($"Jumlah Tambahan = {tambahan}\n");

    // OUTPUT
    mangga += tambahan;
    Console.WriteLine($"Jumlah mangga = {mangga}");


};
static void operatorModulus()
{

    int mangga, orang, hasil;
    Console.WriteLine("== Operator Modulus ==\n");

    // INPUT 
    Console.Write("Masukkan Jumlah Mangga = ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Jumlah orang = ");
    orang = int.Parse(Console.ReadLine());

    // OUTPUT
    hasil = mangga % orang;
    Console.WriteLine($"Sisa Pembagian mangga adalah {hasil}");

};
static void operatorAritmatika() {

    int mangga, apel, hasil ;
    Console.WriteLine("== Operator Aritmatika ==\n");

    // INPUT 
    Console.Write("Masukkan Jumlah Mangga = ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Jumlah Apel = ");
    apel = int.Parse(Console.ReadLine());

    // OUTPUT
    hasil = mangga + apel;
    Console.WriteLine($"Hasil penjumlahan mangga dan apel adalah {hasil}");
    
};

// Fungsi atau method
static void konversi() {
    Console.WriteLine("==KONVERSI==\n");

    Console.Write("Masukkan int = ");
    int myInt = Convert.ToInt32(Console.ReadLine());

    Console.Write("Masukkan double (desimal gunakan ,) = "); // pemisah sesuai settingan windows
    double myDouble = Convert.ToDouble(Console.ReadLine());

    bool myBool = false;


    string strMyInt = Convert.ToString(myInt);
    double dblMyInt = Convert.ToDouble(myInt);
    int intMyDouble = Convert.ToInt32(myDouble);
    string strMyBool = Convert.ToString(myBool);

    Console.WriteLine(strMyInt);
    Console.WriteLine(dblMyInt);
    Console.WriteLine(intMyDouble);
    Console.WriteLine(strMyBool);

};

