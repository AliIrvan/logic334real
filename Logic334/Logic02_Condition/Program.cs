﻿//ifStatements();
//ifElseStatements();
//ifElseIfStatements();
//ifNested();
//ternaryOperator();
switchLogic();

static void switchLogic()
{
    Console.WriteLine("== SWITCH ==\n");
    Console.Write("Pilih buah kesukaan anda (APEL/JERUK/PISANG) : ");

    string pilihan = Console.ReadLine().ToUpper();

    switch (pilihan)
    {
        case "APEL":
            Console.WriteLine("Anda memilih APEL");
            break;
        case "JERUK":
            Console.WriteLine("Anda memilih JERUK");
            break;
        case "PISANG":
            Console.WriteLine("Anda memilih PISANG");
            break;
        default:
            Console.WriteLine("Anda memilih yang lain");
            break;
    }
}
Console.ReadKey();

static void ternaryOperator()
{
    Console.WriteLine("== TERNARY OPERATOR ==\n");
    Console.Write("Masukkan nilai X : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai Y : ");
    int y = int.Parse(Console.ReadLine());

    if (x > y)
    {
        Console.WriteLine("X lebih besar dari Y");
    }
    else if (x < y)
    {
        Console.WriteLine("X Lebih kecil dari Y");
    }
    else
    {
        Console.WriteLine("X sama dengan Y");
    }

    // CARA TERNARY
    Console.WriteLine("\nCara TERNARY");
    string z = x > y ? "X lebih besar dari Y" : x < y ? "X kecil dari Y" : "X Sama dengan Y";
    Console.WriteLine(z);
}
static void ifNested()
{
    Console.WriteLine("== IF NESTED ==\n");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    Console.WriteLine();

    if (nilai >= 75)
    {
        Console.WriteLine("Kamu berhasil !");
        if (nilai == 100)
        {
            Console.WriteLine("dan Nilai kamu sempurna, kamu keren!");
        }
    }
    else
    {
        Console.WriteLine("Kamu gagal !");
    }
}
static void ifElseIfStatements()
{
    Console.WriteLine("== IF ELSE IF STATEMENTS ==\n");

    // INPUT
    Console.Write("Masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.WriteLine();

    // LOGIC OUTPUT
    if (x == 10)
    {
        Console.WriteLine("X is Equals to 10");
    }
    else if (x > 10)
    {
        Console.WriteLine("X is Greater than 10");
    }
    else
    {
        Console.WriteLine("X is lesser than 10");
    }
}
static void ifStatements()
{
    Console.WriteLine("== IF STATEMENTS ==\n");
    Console.Write("Masukkan X : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Y : ");
    int y = int.Parse(Console.ReadLine());
    Console.WriteLine();

    if (x >= 10)
    {
        Console.WriteLine("X besar sama dengan 10");
    }
    if (y <= 5)
    {
        Console.WriteLine("Y kecil sama dengan 5");
    }
};

static void ifElseStatements()
{
    Console.WriteLine("== IF ELSE STATEMENTS ==\n");
    Console.Write("Masukkan nilai X : ");
    int x = int.Parse(Console.ReadLine());
    Console.WriteLine();

    if (x >= 10)
    {
        Console.WriteLine("X besar sama dengan 10");
    }
    else
    {
        Console.WriteLine("X kecil dari 10");
    }
}