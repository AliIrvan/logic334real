﻿// DIMENSION ADN CASE STUDY 
// 1 DIM PRACTICE

//soal1();
//soal2();
//soal3();
//soal4();
//soal5();
//soal6();
//soal7();
//soal8();
//soal9();
//soal10();

// 2 DIM PRACTICE
dim2Soal1();

static void dim2Soal1()
{
    Console.WriteLine("== Soal 1 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan pengali : ");
    int input2 = int.Parse(Console.ReadLine());

    int k = 1 ;
    for (int i = 0; i <= 1; i++)
    {
        for (int j = 0;j < input; j++)
        {
            if (i == 1)
            {
                Console.Write($"{k},   ");
                k *= input2;
                continue;
            }
            Console.Write($"{j},   ");
        }
        Console.WriteLine();
    }
}

static void soal10()
{
    Console.WriteLine("== Soal 10 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 1; i <= input; i++)
    {
        if (i % 4 == 0)
        {
            Console.Write("XXX,   ");
            bilAwal *= 3;
            continue;
        }
        Console.Write($"{bilAwal} ,  ");
        bilAwal *= 3;
    }
}

static void soal9()
{
    Console.WriteLine("== Soal 9 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 1; i <= input; i++)
    {
        if (i % 3 == 0)
        {
            Console.Write("*  ");
            continue;
        }
        Console.Write($"{bilAwal} ,  ");
        bilAwal *= 4;
    }
}

static void soal8()
{
    Console.WriteLine("== Soal 8 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 0; i < input; i++)
    {
        Console.Write($"{bilAwal} ,  ");
        bilAwal *= 3;
    }
}

static void soal7()
{
    Console.WriteLine("== Soal 7 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 0; i < input; i++)
    {
        Console.Write($"{bilAwal} ,  ");
        bilAwal *= 2;
    }
}


static void soal6()
{
    Console.WriteLine("== Soal 6 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 1; i <= input; i++)
    {
        if (i % 3 == 0)
        {
            Console.Write("*  ,");
            bilAwal += 4;
            continue;
        }
        Console.Write($"{bilAwal} ,  ");
        bilAwal += 4;
    }
}
static void soal5()
{
    Console.WriteLine("== Soal 5 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 1; i <= input; i++)
    {
        if (i % 3 == 0)
        {
            Console.Write("*  ,");
            continue;
        }
        Console.Write($"{bilAwal} ,  ");
        bilAwal += 4;
    }
}

static void soal4()
{
    Console.WriteLine("== Soal 4 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 0; i < input; i++)
    {
        Console.Write($"{bilAwal} ,  ");
        bilAwal += 4;
    }
}

static void soal3()
{
    Console.WriteLine("== Soal 3 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 0; i < input; i++)
    {
        Console.Write($"{bilAwal} ,  ");
        bilAwal += 3;
    }
}
static void soal2()
{
    Console.WriteLine("== Soal 2 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 0; i < input; i++)
    {
        Console.Write($"{bilAwal} ,  ");
        bilAwal += 2;
    }
}
static void soal1()
{
    Console.WriteLine("== Soal 1 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 0; i < input; i++)
    {
        Console.Write($"{bilAwal} ,  ");
        bilAwal += 2;
    }
}


