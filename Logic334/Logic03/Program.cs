﻿//perulanganWhile();
//perulanganWhile2();
//perulanganDoWhile();
//forLoop();
//breakLoop();
//continueLoop();
//forNested();
//forEach();
//Text();
//splitAndJoin();
//stringToCharArray();
convertAll();


Console.ReadKey();

static void convertAll()
{
    Console.WriteLine("== CONVERT ALL ==\n");
    Console.Write("Masukkan input angka (dengan batas koma) : ");

    string[] input = Console.ReadLine().Split(",");

    int sum = 0;

    int[] array = Array.ConvertAll(input, int.Parse);

    foreach (int i in array)
    {
        sum += i;
    }
    Console.WriteLine($"Jumlah = {sum}");
}
static void stringToCharArray()
{
    Console.WriteLine("== STRING TO CHAR ARRAY ==\n");
    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();

    char[] arrayKalimat = kalimat.ToCharArray();

    foreach (char c in arrayKalimat)
    {
        Console.WriteLine(c);
    }

    Console.WriteLine();

    for (int i = 0; i < arrayKalimat.Length; i++)
    {
        Console.WriteLine(arrayKalimat[i]);
    }
}
static void splitAndJoin()
{
    Console.Write("== SPLIT AND JOIN ==\n");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukkan pembatas Split : ");
    string pembatasSplit = Console.ReadLine();

    string[] kataKata = kalimat.Split(pembatasSplit);

    foreach (string kata in kataKata)
    {
        Console.WriteLine(kata);
    }

    Console.WriteLine($"Kalimat awal : {string.Join(" + ", kataKata)}");
    Console.WriteLine();
}
static void Text()
{
    Console.WriteLine("== TEXT ==\n");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();

    Console.WriteLine($"Jumlah kata = {kata.Length}");
    Console.WriteLine($"Indeks kata Terakhir = {kata.Length - 1}");
    Console.WriteLine($"kata dalam huruf besar = {kata.ToUpper()}");
    Console.WriteLine($"kata dalam huruf kecil = {kata.ToLower()}");
    Console.WriteLine($"kata dengan 3 huruf terakhir dihapus = {kata.Remove(kata.Length - 3)}");
    Console.WriteLine($"Kata dengan tambahan \"SUPER!!!\" diakhir = {kata.Insert(kata.Length, " SUPER!!!")}");
    Console.WriteLine($"Mengganti kata dengan \"TERGANTI\" = {kata.Replace(kata, "TERGANTI")}");
    Console.WriteLine($"Mengambil kata mulai dari huruf ke 3 sampai 4 huruf setelahnya = {kata.Substring(2, 4)}");
    Console.WriteLine($"Mengambil kata mulai dari huruf ke 5 sampai huruf terakhir = {kata.Substring(4)}");

    Console.Write("Masukan kata yang dilarang : ");
    string kataLarangan = Console.ReadLine();

    if (kata.Contains(kataLarangan) == true)
    {
        Console.WriteLine("kalimat mengandung kata kasar");
    }
    else
    {
        Console.WriteLine("Kalimat mengandung kata kasar");
    }
}
static void forEach()
{
    int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    int sum = 0;

    foreach (int number in numbers)
    {
        sum += number;
    }
    Console.WriteLine($"Jumlah bilangan dalam Array : {sum}\n");

    sum = 0;
    for (int i = 0; i < numbers.Length; i++)
    {
        sum += numbers[i];
    }
    Console.WriteLine($"Jumlah bilangan dalam Array : {sum}\n");
}

static void forNested()
{
    Console.Write("Masukkan nilai = ");
    int input = int.Parse(Console.ReadLine());

    for (int i = 0; i <= input; i++)
    {
        for (int j = 0; j <= input; j++)
        {
            Console.Write($"({i},{j}) ");
        }
        Console.WriteLine();
    }
}
static void breakLoop()
{
    for (int i = 0; i < 10; i++)
    {
        if (i == 5)
        {
            break;
        }
        Console.WriteLine(i);
    }
}

static void continueLoop()
{
    for (int i = 0; i < 10; i++)
    {
        if (i == 5)
        {
            continue;
        }
        Console.WriteLine(i);
    }
}

static void forLoop()
{
    Console.WriteLine("== PERULANGAN FOR ==\n");
    Console.Write("Masukkan nilai : ");
    int number = int.Parse(Console.ReadLine());

    for (int i = 0; i < number; i++)
    {
        Console.WriteLine($"Nilai = {i}");
    }

    for (int i = number; i >= 0; i--)
    {
        Console.WriteLine($"Nilai = {i}");
    }
}
static void perulanganDoWhile()
{
    Console.WriteLine("==  DO WHILE LOOP ==\n");

    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    do
    {
        Console.WriteLine($"Proses ke : {nilai}");
        nilai++;
    } while (nilai < 6);
}
static void perulanganWhile2()
{
    Console.WriteLine("== PERULANGAN WHILE 2 ==\n");
    Console.Write("Masukkan Nilai = ");
    int nilai = int.Parse(Console.ReadLine());

    bool ulang = true;

    while (ulang == true)
    {
        Console.WriteLine($"Nilai = {nilai}");
        nilai++;

        Console.Write("ulangi proses ? (Y/N) = ");
        string input = Console.ReadLine();

        if (input.ToUpper() == "N")
        {
            ulang = false;
        }
    }

}
static void perulanganWhile()
{
    Console.WriteLine("== Perulangan While ==\n");
    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while (nilai < 6)
    {
        Console.WriteLine($"Nilai = {nilai}");
        nilai++;
    }

}