﻿

//tugas1();
//tugas2a();
//tugas2();
//tugas3();
//tugas4();
//tugas5();
//tugas6();
//tugas6BubbleSorting();
//tugas7();
tugas7b();

﻿
Console.ReadKey();
static void tugas1()
{
    Console.WriteLine("== PROGRAM DERET ==\n");
    Console.Write(" Masukkan banyak bilangan : ");
    int batas = int.Parse(Console.ReadLine());
    Console.Write(" Masukkan Pengali : ");
    int kali = int.Parse(Console.ReadLine());

    for (int i = 1; i <= batas; i++)
    {
        if (i % 2 == 0)
        {
            Console.Write($"{i * kali}   ");
            continue;
        }

        Console.Write($"{i * -1 * kali}   ");
    }
}
static void tugas2()
{
    Console.WriteLine("== PROGRAM CONVERT 12 JAM TO 24 JAM ==\n");


    Console.Write("Masukan jam  (h:m:s AM/PM) = ");
    
    string timeString = Console.ReadLine();
    
    //if (!timeString.Contains(" "))
    //{
    //    timeString = timeString.Insert(timeString.Length - 2," ");
    //}

    try
    {
        DateTime time24 = Convert.ToDateTime(timeString);
        var timeKonversi = time24.ToString("HH:mm:ss");
        Console.WriteLine($"Jam Konversi = {timeKonversi}");
    }
    catch (Exception ex)
    {
        Console.WriteLine("Format yang anda masukkan salah !");
        Console.WriteLine($"Pesan Error : {ex.Message}");
    }

    //Console.Write("Masukan jam (h:m:s) = ");
    //string timeString = Console.ReadLine();

    Console.Write("Masukkan Kode Waktu (AM/PM) = ");
    string kodeWaktu = Console.ReadLine().ToUpper();

    Console.WriteLine();

    try
    {
        TimeOnly inputTime = TimeOnly.ParseExact(timeString, "h:m:s", null);

        if (kodeWaktu == "PM")
        {
            inputTime = inputTime.Add(new TimeSpan(12, 0, 0));
            Console.WriteLine($"Jam Konversi = {inputTime.ToString("hh:mm:ss")}");
        }
        else
        {
            Console.WriteLine($"Jam Konversi = {inputTime}");
        }

    }
    catch (Exception ex)
    {
        Console.WriteLine("Format yang anda masukkan salah !");
        Console.WriteLine($"Pesan Error : {ex.Message}");
    }
}

static void tugas3()
{
    Console.WriteLine("== PROGRAM HARGA BAJU ==\n");
    Console.Write("Masukkan Kode Baju (1/2/3)          : ");
    int kodeBaju = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kode Ukuran (S/M/Lainnya)  : ");
    char kodeUkuran = char.Parse(Console.ReadLine().ToUpper());

    string merkBaju = "";
    int harga = 0;

    if (kodeBaju == 1)
    {
        merkBaju = "IMP";

        if (kodeUkuran == 'S')
        {
            harga = 200000;
        }
        else if (kodeUkuran == 'M')
        {
            harga = 220000;
        }
        else
        {
            harga = 250000;
        }
    }
    else if (kodeBaju == 2)
    {
        merkBaju = "Prada";

        if (kodeUkuran == 'S')
        {
            harga = 150000;
        }
        else if (kodeUkuran == 'M')
        {
            harga = 160000;
        }
        else
        {
            harga = 170000;
        }
    }
    else if (kodeBaju == 3)
    {
        merkBaju = "Gucci";

        if (kodeUkuran != null)
        {
            harga = 200000;
        }
    }
    else
    {
        Console.WriteLine(" Kode Baju Tidak Valid!");
    }

    Console.WriteLine($"\nMerk Baju   = {merkBaju}");

    Console.WriteLine($"Harga       = {harga.ToString("#,#.00")}");

    Console.WriteLine($"Harga       = {harga}");
}

static void tugas4()
{

    Console.WriteLine("== Program Beli Baju ==\n");

    Console.Write("Masukkan Jumlah Uang         = ");
    int uang = int.Parse(Console.ReadLine());

    Console.Write("Masukkan List Harga Baju  (pemisah: ,)         = ");
    string strhargaBaju = Console.ReadLine();
    
    string[] hargaBaju = strhargaBaju.Split(",");
    int[] listHargaBaju = Array.ConvertAll(hargaBaju, int.Parse);

    Console.Write("Masukkan List Harga Celana (pemisah: ,)        = ");
    string strhargaCelana = Console.ReadLine();
    string[] hargaCelana = strhargaCelana.Split(",");
    int[] listHargaCelana = Array.ConvertAll(hargaCelana, int.Parse);
   
    int[,] totalBelanja = new int[listHargaBaju.Length, listHargaCelana.Length];
    List<int> bestProduct = new List<int>() { };
    

    for (int i = 0; i < totalBelanja.GetLength(0); i++)
    {
        for (int j = 0; j < totalBelanja.GetLength(1); j++)
        {
            totalBelanja[i, j] = listHargaBaju[i] + listHargaCelana[j];

            if (totalBelanja[i, j] > uang)
            {
                totalBelanja[i, j] = 0;
            }

            bestProduct.Add(Convert.ToInt32((totalBelanja[i, j])));
        }
    }

    Console.WriteLine("\njumlah belanja yang dapat di beli : ");
    for (int i = 0; i < totalBelanja.GetLength(0); i++)
    {
        for (int j = 0; j < totalBelanja.GetLength(1); j++)
        {
            Console.WriteLine($"kombinasi [{i},{j}] = {totalBelanja[i, j]}");

        }
    }
    Console.Write($"\nPenawaran terbaik = {bestProduct.Max()}");
}

static void tugas5()
{
    Console.WriteLine("== PROGRAM ARRAY ==\n");

    Console.Write("Masukan Nilai Array : ");
    string input = Console.ReadLine();

    string[] array = input.Split(",");

    Console.Write("Masukan Jumlah Rotasi : ");
    int rotasi = int.Parse(Console.ReadLine());

    Console.WriteLine();
    Console.WriteLine($" Susunan Array Awal = [{String.Join(",", array)}]");
    Console.WriteLine();

    for (int loop = 1; loop <= rotasi; loop++)
    {
        string arrayFirst = array[0];
        for (int i = 0; i < array.Length - 1; i++)
        {
            array[i] = array[i + 1];
        }
        array[array.Length - 1] = arrayFirst;
        Console.WriteLine($" Susunan Array ke {loop} = [{String.Join(",", array)}]");
    }
    Console.WriteLine($" Susunan Array Baru= [{String.Join(",", array)}]");
}


static void tugas6()
{
    Console.WriteLine("== Sorting ARRAY ==\n");

    Console.Write("Masukan Nilai Array : ");
    string input = Console.ReadLine();
    string[] array = input.Split(",");

    Console.WriteLine($"Array awal = [ {String.Join(",", array)}]");

    Array.Sort(array);

    Console.WriteLine($"Array Sorted = [ {String.Join(",", array)}]");
}

 static void tugas6BubbleSorting()
{
    Console.WriteLine("== BUBBLE SORTING ==  ");
   
    Console.Write("Masukan Nilai Array : ");
    string input = Console.ReadLine();
    string[] array = input.Split(",");
    int[] intArray = Array.ConvertAll(array, int.Parse);

    for(int i = 0;i < intArray.Length - 1; i++)
    {
        for (int j = 0; j < intArray.Length - i - 1; j++)
        {
            if (intArray[j] > intArray[j + 1])
            {
                int tempNumber = intArray[j];
                intArray[j] = intArray[j + 1];
                intArray[j + 1] = tempNumber;

                Console.Write($"Kondisi Array : ");
                foreach (int number in intArray)
                {
                    Console.Write($"{number} , ");
                }
                Console.WriteLine();
            }
        }
    }

    Console.Write("\nArray Sorted = ");
    foreach (int i in intArray)
    {
        Console.Write($"{i} , ");
    }


}

static void tugas7()
{
    Console.WriteLine("== Soal DIM ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Penambah : ");
    int bilPenambah = int.Parse(Console.ReadLine());

    for (int i = 0; i < input; i++)
    {
        Console.Write($"{bilAwal} ,  ");
        bilAwal += bilPenambah;
    }

}

static void tugas7b()
{
    Console.WriteLine("== Soal DIM 05 ==\n");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Bilangan Awal : ");
    int bilAwal = int.Parse(Console.ReadLine());

    for (int i = 1; i <= input; i++)
    {
        if (i % 3 == 0)
        {
            Console.Write("* ");
            continue;
        }
        Console.Write($"{bilAwal} ");
        bilAwal += 4;
    }

}




