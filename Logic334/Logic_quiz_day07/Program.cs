﻿//tugas1();
//tugas2();
//tugas3();
//tugas4();
//tugas5();
//tugas6();
//tugas7();
tugas8();
//tugas9();
//tugas10();




Console.ReadKey();

static void tugas2B()
{
    Console.WriteLine("== PROGRAM PERBANDINGAN ARRAY ==\n");

    Console.Write("Masukkan Pesan        : ");
    string message = Console.ReadLine().ToUpper();
    Console.Write("Masukkan Kata Kunci   : ");
    string keyWord = Console.ReadLine().ToUpper();

    List<bool> check = new List<bool>();

    if (message.Contains(keyWord))
    {
        check.Add(true);
    }
}
static void tugas1()
{
    Console.WriteLine("== Program factorial ==\n");
    Console.Write(" Masukkan Input Angka : ");
    int input = int.Parse(Console.ReadLine());
    int result = 1;
    string detail = "";


    if (input == 0)
    {
        result = 1;
    }
    else
    {
        for (int i = 1; i <= input; i++)
        {
            result *= i;
            detail += detail == "" ? i : " x " + i; // cetak tambahan keternagan isi factorial
        }
    }
    Console.WriteLine($"\n Jumlah cara = {detail} = {result}");
}

static void tugas2()
{
    Console.WriteLine("== PROGRAM PERBANDINGAN ARRAY ==\n");

    Console.Write("Masukkan Pesan        : ");
    string message = Console.ReadLine().ToUpper();
    Console.Write("Masukkan Kata Kunci   : ");
    string keyWord = Console.ReadLine().ToUpper();

    // Mengambil substring per 3 huruf
    int jumHuruf = 3;
    List<string> messageCacah = new List<string>();
    List<string> keyWordCacah = new List<string>();
    List<bool> isEqual = new List<bool>();

    for (int i = 0; i < message.Length; i += jumHuruf)
    {
        messageCacah.Add(message.Substring(i, jumHuruf));
    }

    for (int i = 0; i < keyWord.Length; i += jumHuruf)
    {
        keyWordCacah.Add(keyWord.Substring(i, jumHuruf));
    }

    Console.WriteLine();
    Console.WriteLine($" Mesagge Cacah = {string.Join(",", messageCacah)}");
    Console.WriteLine($" KeyWord Cacah = {string.Join(",", keyWordCacah)}");
    Console.WriteLine();


    for (int i = 0; i < messageCacah.Count; i++)
    {
        if (messageCacah[i] == keyWordCacah[i])
        {
            isEqual.Add(true);
        }
        else if (messageCacah[i] != keyWordCacah[i])
        {
            isEqual.Add(false);
        }

    }

    int CountTrue = 0;
    int CountFalse = 0;

    foreach (bool data in isEqual)
    {
        if (data == true)
        {
            CountTrue++;
        }
        else if (data == false)
        {
            CountFalse++;
        }
    }

    Console.WriteLine($"Jumlah kode yang benar = {CountTrue}");
    Console.WriteLine($"Jumlah kode yang salah = {CountFalse}");
}

static void tugas3()
{
    Console.WriteLine("== Program Denda Buku ==\n");
    // INPUT
    Console.WriteLine("Format Penanggalan : d/M/yyyy ");

    try
    {
        Console.Write("Masukkan tanggal peminjaman                = ");
        DateTime pinjam = DateTime.Parse(Console.ReadLine());
        Console.Write("Masukkan lama peminjaman dalam hari        = ");
        int HariPinjam = int.Parse(Console.ReadLine());
        Console.Write("Masukkan tanggal pengembalian              = ");
        DateTime pengembalian = DateTime.Parse(Console.ReadLine());
        Console.Write("Masukkan denda keterlambatan               = ");
        int denda = int.Parse(Console.ReadLine());

        // Penghutangan tanggal pengembalian
        DateTime jatuhTempo = pinjam.AddDays(HariPinjam);

        // Penghitungan jumlah hari keterlambatan
        TimeSpan intervalKeterlambatan = pengembalian - jatuhTempo;

        // mencari total denda
        int jumlahHari = intervalKeterlambatan.Days;
        if (intervalKeterlambatan.Days < 0)
        {
            jumlahHari = 0;
        }
        int totalDenda = denda * jumlahHari;

        // OUTPUT
        Console.Write($"\nTotal denda keterlambatan selama {jumlahHari} hari    = {totalDenda:C}");
    }
    catch (Exception ex)
    {
        Console.WriteLine("Format yang anda masukkan salah !");
        Console.WriteLine($"Pesan Error : {ex.Message}");
    }

}

static void tugas4()
{
    Console.WriteLine("== Program FT ==\n");


    Console.Write("Masukan Tanggal Mulai (d/M/yyy)              : ");
    DateTime tanggalMulai = DateTime.Parse(Console.ReadLine());

    Console.Write("Masukan jumlah hari training                 : ");
    int train = int.Parse(Console.ReadLine());

    Console.Write("Masukan tanggal libur (gunakan pemisah : , ) : ");
    string[] libur = Console.ReadLine().Split(",");

    DateTime[] holiday = Array.ConvertAll(libur, DateTime.Parse);

    DateTime tanggalSekarang = tanggalMulai;
    DateTime tanggalFT;

    for (int i = 0; i < train; i++)
    {
        if (holiday.Contains(tanggalSekarang))
        {
            train++;
            tanggalSekarang = tanggalSekarang.AddDays(1);
        }
        else if (tanggalSekarang.DayOfWeek == DayOfWeek.Saturday || tanggalSekarang.DayOfWeek == DayOfWeek.Sunday)
        {
            train++;
            tanggalSekarang = tanggalSekarang.AddDays(1);
        }
        else
        {
            tanggalSekarang = tanggalSekarang.AddDays(1);
        }
    }
    tanggalFT = tanggalSekarang;

    Console.WriteLine($"FT akan diadakan pada : {tanggalFT.ToString("dddd, dd/MM/yyyy")}");

}

static void tugas5()
{
    Console.WriteLine("== PROGRAM VOKAL KONSONAN ==\n");

    Console.Write("Masukkan kalimat : ");
    Char[] teks = Console.ReadLine().ToUpper().ToCharArray();

    // Menghapus nilai kosong dari CharArray
    teks = teks.Where(x => !Char.IsWhiteSpace(x)).ToArray();

    char[] hurufVokal = { 'A', 'I', 'U', 'E', 'O' };
    int konsonan = 0;
    int vokal = 0;

    for (int i = 0; i < teks.Length; i++)
    {
        if (hurufVokal.Contains(teks[i]))
        {
            vokal++;
        }
        else
        {
            konsonan++;
        }
    }

    Console.WriteLine($"Total Huruf Konsonan = {konsonan}");
    Console.WriteLine($"Total Huruf vokal    = {vokal}");
}

static void tugas6()
{
    Console.WriteLine("== PROGRAM CHAR ARRAY ==\n");

    Console.Write("Masukkan kalimat : ");
    Char[] teks = Console.ReadLine().ToLower().ToCharArray();

    foreach (char c in teks)
    {
        if (Char.IsLetter(c))
        {
            Console.WriteLine($"***{c}***");
        }
    }
}

static void tugas7()
{
    Console.WriteLine("== PROGRAM MENU ==\n");

    Console.Write("Total Menu                   : ");
    int totalMenu = int.Parse(Console.ReadLine());
    Console.Write("Indeks Makanan ELergi        : ");
    int alergi = int.Parse(Console.ReadLine());
    Console.Write("Harga Menu ( , )             : ");
    int[] menu = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
    Console.Write("Uang Elsa                    : ");
    int uangElsa = int.Parse(Console.ReadLine());

    Console.Write("List Pesanan pengunjung      : ");
    int[] order = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
    Console.WriteLine($"/nList Harga Pesanan = {String.Join(",", order)}");
    Console.WriteLine($"Total Pesanan      = {order.Sum()}");

    List<int> bill = new List<int>();
    for (int i = 0; i < menu.Length; i++)
    {
        if (order[i] == menu[alergi])
        {
            order[i] = 0;
        }
        bill.Add(order[i]);
    }

    Console.WriteLine($"Makanan yang bisa Elsa makan  = {bill.Sum()}");
    Console.WriteLine($"Pesanan yang harus Elsa bayar = {bill.Sum() / 2}");

    int sisaUangElsa = uangElsa - (bill.Sum() / 2);
    if (sisaUangElsa < 0)
    {
        Console.WriteLine($"Uang Elsa kurang sebesar                = {Math.Abs(sisaUangElsa)}");
    }
    else if (sisaUangElsa > 0)
    {
        Console.WriteLine($"Uang Elsa bersisa sebesar               = {sisaUangElsa}");
    }
    else
    {
        Console.WriteLine($"Uang Elsa pas");
    }
}

static void tugas8()
{
    Console.WriteLine("== PROGRAM PYRAMID ==\n");

    Console.Write("Masukan input angka : ");
    int input = int.Parse(Console.ReadLine());
    /*
    for (int i = 0; i <= input; i++)
    {
        for (int j = 0; j <= input; j++)
        {
            if (j >= input)
            {
                Console.Write("*");
            }
            else
            {
                Console.Write(" ");
            }
        }
        Console.WriteLine();
    }
    */

    for (int i = 0; i <= input; i++)
    {
        Console.WriteLine(new string(' ', input - i) + new string('*', i));
    }
}

static void tugas9()
{
    Console.WriteLine("== PROGRAM ARRAY DIAGONAL ==\n");

    Console.Write("Msukkan jumlah kolom (baris = kolom) : ");
    int batas = int.Parse(Console.ReadLine());

    int[,] array2D = new int[batas, batas];
    int[] arraykolom = Array.Empty<int>();
    List<int> kolom = new List<int>();

    for (int i = 0; i < batas; i++)
    {
        Console.Write($"Masukan data baris ke {i + 1} : ");
        arraykolom = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

        foreach (int number in arraykolom)
        {
            kolom.Add(number);
        }

        for (int j = 0; j < batas; j++)
        {
            array2D[i, j] = kolom[j];
        }

        kolom.RemoveAll(x => x != null);
    }

    Console.WriteLine("\nArray 2 Dimensi = ");

    for (int i = 0; i < batas; i++)
    {
        Console.Write("{ ");
        for (int j = 0; j < batas; j++)
        {
            Console.Write($"{array2D[i, j]} ");
        }
        Console.WriteLine(" }");
    }

    // Mencari perbedaan diagonal 
    int diagonal1 = 0;
    int diagonal2 = 0;

    for (int i = 0; i < batas; i++)
    {
        for (int j = 0; j < batas; j++)
        {
            if (i == j)
            {
                diagonal1 += array2D[i, j];
            }

            if (j == batas - -1 - i)
            {
                diagonal2 += array2D[i, j];
            }
        }
    }

    int interval = Math.Abs(diagonal1 - diagonal2);
    Console.WriteLine($"\nTotal Diagonal 1         = {diagonal1}");
    Console.WriteLine($"Total Diagonal 2         = {diagonal2}");
    Console.WriteLine($"Perbedaan Nilai Diagonal = {interval}");

}

static void tugas10()
{
    Console.WriteLine("== PROGRAM TIUP LILIN ==\n");

    Console.Write("Masukan tinggi lilin : ");
    int[] input = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

    int maxLilin = input.Max();
    int count = 0;
    for (int i = 0; i < input.Length; i++)
    {
        if (input[i] == maxLilin)
        {
            count++;
        }
    }

    Console.WriteLine($"Lilin yang tertinggi = {maxLilin}");
    Console.WriteLine($"Jumlah lilin tertiup = {count}");
}

