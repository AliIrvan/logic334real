﻿using Logic04;

//inisializeArray();
//mengaksesElementArray();
array3Dimensi();
//inisializeList();
//panggilClassStudent();
//mengaksesElementList();
//indexElementList();
//inisializeDateTime();
//parsingDateTime();
//dateTimeProperties();
//timeSpan();
Console.ReadKey();

static void timeSpan()
{
    Console.WriteLine("== TIMESPAN ==\n");

    //Inisialisasi waktu
    DateTime dateNow = DateTime.Now;
    DateTime dateBirth = new DateTime(1997, 08, 12, 02, 00, 00);

    //Perhitungan TimeSpan
    TimeSpan interval = dateNow - dateBirth;

    //Cetak Output
    int umurTahun = interval.Days / 365;
    double umurBulan = (interval.Days % 365) / 12;
    double umurHari = (interval.Days % 365) / 30;
    Console.WriteLine($" Usia Sekarang              : {umurTahun} tahun, {umurBulan} bulan, {umurHari} hari");
    Console.WriteLine($" Jarak hari                 : {interval.Days}");
    Console.WriteLine($" Jarak hari total           : {interval.TotalDays}");
    Console.WriteLine($" Jarak jam                  : {interval.Hours}");
    Console.WriteLine($" Jarak jam Total            : {interval.TotalHours}");
    Console.WriteLine($" Jarak menit                : {interval.Minutes}");
    Console.WriteLine($" Jarak menit Total          : {interval.TotalMinutes}");
    Console.WriteLine($" Jarak detik                : {interval.Seconds}");
    Console.WriteLine($" Jarak detik Total          : {interval.TotalSeconds}");
    Console.WriteLine($" Jarak miliseconds          : {interval.Milliseconds}");
    Console.WriteLine($" Jarak milliseconds Total   : {interval.TotalMilliseconds}");

    Console.WriteLine();


}
static void dateTimeProperties()
{
    Console.WriteLine("== Date Time Properties ==\n");
    DateTime myDate = new DateTime(2023, 11, 11, 11, 10, 25);

    int tahun = myDate.Year;
    int bulan = myDate.Month;
    int hari = myDate.Day;
    int jam = myDate.Hour;
    int menit = myDate.Minute;
    int detik = myDate.Second;
    int weekDay = (int)myDate.DayOfWeek;
    string hariString = myDate.ToString("dddd");
    string hariString2 = myDate.DayOfWeek.ToString("dddd");

    Console.WriteLine($"Tahun       = {tahun}");
    Console.WriteLine($"Bulan       = {bulan}");
    Console.WriteLine($"hari        = {hari}");
    Console.WriteLine($"Jam         = {jam}");
    Console.WriteLine($"Menit       = {menit}");
    Console.WriteLine($"Detik       = {detik}");
    Console.WriteLine($"Weekday     = {weekDay}");
    Console.WriteLine($"haristring  = {hariString}");
    Console.WriteLine($"haristring2 = {hariString2}");

}
static void parsingDateTime()
{
    Console.WriteLine("== PARSING DATE TIME ==\n");
    Console.Write("Masukkan tanggal (d/MM/yyyy) : ");
    string dateString = Console.ReadLine();

    try
    {
        DateTime dt1 = DateTime.ParseExact(dateString, "d/MM/yyyy", null);
        Console.WriteLine(dt1);
    }
    catch (Exception ex)
    {
        Console.WriteLine("Format yang anda masukkan salah !");
        Console.WriteLine($"Pesan Error : {ex.Message}");
    }

}
static void inisializeDateTime()
{
    Console.WriteLine("== INISIALISASI DATETIME ==\n");

    DateTime dt1 = new DateTime(); // 01/01/0001, 00:00:00.000
    Console.WriteLine(dt1);

    DateTime dtNow = DateTime.Now; // Tanggal dan waktu sekarang
    Console.WriteLine(dtNow);
    Console.WriteLine(dtNow.ToString("dddd, dd - MMMM / yyyy, h, mm, ss"));

    DateTime dt2 = new DateTime(2023, 11, 1);
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2023, 11, 1, 10, 43, 15);
    Console.WriteLine(dt3);


}
static void indexElementList()
{
    Console.WriteLine("== Index Element List ==\n");

    List<string> list = new List<string>();
    list.Add("1");
    list.Add("2");
    list.Add("3");

    Console.Write("Masukkan data Element : ");
    string item = Console.ReadLine();

    int index = list.IndexOf(item);

    if (index != -1)
    {
        Console.WriteLine($"Element {item} is found at index {index}, Element will be remove from the list");
        list.Remove(item);
    }
    else
    {
        Console.WriteLine($"Element {item} is not found, Element will be add to the list");
        list.Add(item);
    }

    Console.WriteLine("\n item list :");
    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(i + ": " + list[i]);
    }
}
static void mengaksesElementList()
{
    Console.WriteLine("== Mengakses Element List ==\n");

    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    // INSERT LIST :
    list.Insert(0, 0);

    Console.WriteLine(list[0]);
    Console.WriteLine(list[1]);
    Console.WriteLine(list[2]);

    Console.WriteLine();

    foreach (int item in list)
    {
        Console.WriteLine(item);
    }

    Console.WriteLine();

    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}

static void panggilClassStudent()
{
    Console.WriteLine("== Panggil Class Student ==\n");

    // memanggil Class Student
    //Student student = new Student();

    List<Student> students = new List<Student>()
    {
        new Student() { Id = 1, Name = "Jhon Doe"},
        new Student() { Id = 2, Name = "Jane Doe"},
        new Student() { Id = 3, Name = "Joe Doe"}
    };

    // Menambah data :
    Student student = new Student();
    student.Id = 5;
    student.Name = "Ginny Doe";

    students.Add(new Student() { Id = 4, Name = "Grammy Doe" });
    students.Add(student);


    Console.WriteLine($"Panjang List students = {students.Count}");

    foreach (Student item in students)
    {
        Console.WriteLine($"Id : {item.Id} , Name : {item.Name} ");
    }

    Console.WriteLine();
    for (int i = 0; i < students.Count; i++)
    {
        Console.WriteLine($"Id : {students[i].Id} , Name : {students[i].Name} ");
    }

}
static void mengaksesElementArray()
{
    Console.WriteLine("== Mengakses Array ==\n");

    int[] array = new int[3];

    // ISI DATA
    array[0] = 1;
    array[1] = 2;
    array[2] = 3;

    // Mengambil data Array
    Console.WriteLine(array[0]);
    Console.WriteLine(array[1]);
    Console.WriteLine(array[2]);
    Console.WriteLine();

    // mengambil data dengan looping
    int[] array2 = { 1, 2, 3, 4, 5 };

    for (int i = 0; i < array2.Length; i++)
    {
        Console.WriteLine(array2[i]);
    }
    Console.WriteLine();

    string[] array3 = { "A", "B", "C", "D" };
    foreach (string str in array3)
    {
        Console.Write($"{str} + ");
    }
}
static void inisializeArray()
{
    Console.WriteLine("== INISIALISASI ARRAY ==\n");

    // Cara 1
    int[] array = new int[5];

    // Cara 2
    int[] array2 = new int[5] { 1, 2, 3, 4, 5 };

    // Cara 3
    int[] array3 = new int[] { 1, 2, 3, 4, 5 };

    // Cara 4
    int[] array4 = { 1, 2, 3, 4, 5 };

    // Cara 5
    int[] array5;
    array5 = new int[] { 1, 2, 3, 4, 5 };

    Console.WriteLine($"cetak Array 1 = {string.Join(" , ", array)}");
    Console.WriteLine($"cetak Array 2 = {string.Join(" , ", array2)}");
    Console.WriteLine($"cetak Array 3 = {string.Join(" , ", array3)}");
    Console.WriteLine($"cetak Array 4 = {string.Join(" , ", array4)}");
    Console.WriteLine($"cetak Array 5 = {string.Join(" , ", array5)}");

}


static void array3Dimensi()
{
    Console.WriteLine("== Array 3 Dimensi ==\n");

    int[,] array = new int[3, 3]
    {
        { 1, 2, 3 },
        { 4, 5, 6 },
        { 7, 8, 9 }
    };

    for (int i = 0; i < array.GetLength(0); i++)
    {
        for (int j = 0; j < array.GetLength(1); j++)
        {
            Console.Write($"{array[i, j]}   ");
        }
        Console.WriteLine();
    }
}

static void inisializeList()
{
    Console.WriteLine("== INISIALISASI LIST ==\n");

    List<string> list = new List<string>()
    {
        "Jhon Doe",
        "Jane Doe",
        "Joe Doe"
    };

    // Menambah data
    list.Add("Jinny Doe");

    // Edit  data
    list[list.Count - 1] = "Grammy Doe";

    // Menghapus data
    list.RemoveAt(0);
    list.Remove(""); // Walapun data tidak ada yang match, tidak menghasilkan error

    Console.WriteLine(string.Join(", \n", list));
}

