﻿// Input
Console.Write("Masukkan nama = ");

string nama = Console.ReadLine();
string kelas = "Batch 334";

// Output
Console.WriteLine();
Console.WriteLine($"hi {nama}, Selamat datang di {kelas}");

// 2 cara lain cetak String
Console.WriteLine("hi " + nama + ", Selamat datang !");
Console.WriteLine("hi {0}, Selamat Datang !", nama);
Console.WriteLine();

// Data Types Eksplisit
string alamat = "kemayoran";
int umur = 25;
bool isMenikah = false;
const float phi = 3.14F; // value tidak dapat  berubah

// Data Type Implisit
var nilai = 5; // tipe data nilai tergantung otomatis kepada value nya

// Konversi Data Types
// Convert.ToString(umur);

// mengganti isi variabel
alamat = "Langsat III";

Console.WriteLine(alamat);


Console.ReadKey();

/*
    Hal yang harus dihindari dalam membuat nama variable
 * Tidak di awali dengan angka atau simbol
 * harus unik
 * dianjurkan camelCase
 
    var : variable yang dpat diganti
    char : variable yang tidak dapat diganti
  
  
 */