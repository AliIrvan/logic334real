﻿using Logic06;

//abstractClass();
//objectClass();
//constraktor();
//Encapsulation();
//Inheritance();
Overriding();


Console.ReadKey();

static void Overriding()
{
    Console.WriteLine("== OVERRIDING ==\n");

    Kucing kucing = new Kucing();
    Paus paus = new Paus();

    Console.WriteLine($"Kucing {kucing.Pindah()}");
    Console.WriteLine($"Paus {paus.Pindah()}");
}

static void Inheritance()
{
    Console.WriteLine("== INHERITANCE ==\n");

    TypeMobil sedan = new TypeMobil();
    
    sedan.Civic();
}
static void Encapsulation()
{
    Console.WriteLine("== Encapsulation ==\n");
    Persegipanjang pp = new Persegipanjang();
    pp.panjang = 4.5;
    pp.lebar = 5.5;
    pp.tampilkanLuas();
}
static void constraktor()
{
    Console.WriteLine("== Constaktor ==\n");

    Mobil toyota = new Mobil("BA 1234 LN");
    string platno = toyota.getPlatNo();
    //string platno = toyota.platNo;

    Console.WriteLine($"Mobil Toyota dengan nomor polisi : {platno}");

    
}
static void objectClass()
{
    Console.WriteLine("== OBJECT CLASS ==\n");

    Mobil toyota = new Mobil()
    {
        nama = "Avanza",
        kecepatan = 0,
        bensin = 10,
        posisi = 0
    };

    toyota.percepat();
    toyota.maju();
    toyota.isiBensin(20);
    toyota.utama();

}
static void abstractClass()
{
    Console.WriteLine("== Abstrack ==\n");

    Console.Write("Masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai y : ");
    int y = int.Parse(Console.ReadLine());

    testTurunan cale = new testTurunan(); // cale adalah objek
    int jumlah = cale.jumlah(x, y);
    int kurang = cale.kurang(x, y);

    Console.WriteLine($"Hasil {x} + {y} = {jumlah}");
    Console.WriteLine($"Hasil {x} - {y} = {kurang}");
}