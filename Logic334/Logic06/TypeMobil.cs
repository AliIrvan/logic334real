﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class TypeMobil : Mobil
    {
        public void Civic()
        {
            nama = "Honda Civic";
            platNo = "BA 1234 LN";
            bensin = 10;
            kecepatan = 10;
            maju();
            utama();
        }
    }
}
