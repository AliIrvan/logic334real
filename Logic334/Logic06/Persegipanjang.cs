﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class Persegipanjang
    {
        public double panjang;
        public double lebar;
        public double luasPersegiPanjang()
        {
            return panjang * lebar;
        }
        public void tampilkanLuas()
        {
            Console.WriteLine($" Luas = {luasPersegiPanjang}");
        }
    }
}
