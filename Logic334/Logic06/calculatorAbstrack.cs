﻿namespace Logic06
{
    public abstract class calculatorAbstrack
    {
        public abstract int jumlah(int x, int y);
        public abstract int kurang(int x, int y);
    }

    public class testTurunan : calculatorAbstrack
    {
        public override int jumlah(int x, int y)
        {
            return x + y;
        }
        public override int kurang(int x, int y)
        {
            return x - y;
        }
    }
}
