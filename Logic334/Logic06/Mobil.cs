﻿namespace Logic06
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        public string nama;
        public string platNo;

        // CONSTRAKTOR
        public Mobil(string platNo)
        {
            this.platNo = platNo;
        }

        // CONSTRAKTOR 2
        public Mobil() { }

        public string getPlatNo()
        {
            return platNo;
        }
        public void utama()
        {
            Console.WriteLine($"Nama        = {nama}");
            Console.WriteLine($"Nomor Polisi= {platNo}");
            Console.WriteLine($"Bensin      = {bensin}");
            Console.WriteLine($"Kecepatan   = {kecepatan}");
            Console.WriteLine($"posisi      = {posisi}");
        }
        public void percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }

        public void maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }

        public void isiBensin(double bensin)
        {
            this.bensin += bensin;
        }
    }
}
