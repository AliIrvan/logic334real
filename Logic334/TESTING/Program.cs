﻿//test1();
test2();
static void test2()
{
    Console.Write("input : ");
    string teks = Console.ReadLine().ToLower();

    List<char> vokal = new List<char>();
    List<char> konsonan = new List<char>();

    List<char> listvokal = new List<char> { 'a', 'i', 'u', 'e' ,'o'};

    foreach(char c in teks)
    {
        if (listvokal.Contains(c))
        {
            vokal.Add(c);
        }
        else
        {
            konsonan.Add(c);
        }
    }

    Console.WriteLine(String.Join("",vokal));
    Console.WriteLine(String.Join("",konsonan.OrderBy(x => x)));
}
static void test1()
{
    Console.Write("input : ");
    int n = int.Parse(Console.ReadLine());
    List<int> ganjil = new List<int>();
    List<int> genap = new List<int>();

    for (int i = 1; i <= n; i++)
    {
        if (i % 2 == 0)
        {
            genap.Add(i);
        }
        else
        {
            ganjil.Add(i);
        }
    }
    Console.WriteLine(String.Join(" ",ganjil.OrderBy(x => x)));
    Console.WriteLine(String.Join(" ",genap.OrderBy(x => x)));
}