-- SQL DAY 03
SELECT * FROM vw_mahasiswa

-- CREATE Stored Procedure => SP
CREATE PROCEDURE SP_RetrieveMahasiswa AS
BEGIN
	SELECT 
		name, address, email, nilai
	FROM
		mahasiswa
END

-- menjalankan procedure
EXEC SP_RetrieveMahasiswa

-- Edit / Alter SP
ALTER PROCEDURE SP_RetrieveMahasiswa 
	@id INT,
	@name VARCHAR(50)
AS
BEGIN
	SELECT 
		name, address, email, nilai
	FROM
		mahasiswa
	WHERE
		id = @id AND name = @name
END

-- menjalankan procedure dengan parameter
EXEC [SP_RetrieveMahasiswa] 2, 'Imam'

-- FUNCTION
-- Create FUNCTION
CREATE FUNCTION FN_totalMahasiswa(@id INT)
RETURNS INT
BEGIN
	DECLARE @hasil INT
	SELECT @hasil = COUNT(id) FROM mahasiswa WHERE id = @id
	RETURN @hasil
END

-- menjalankan FUNCTION
SELECT dbo.FN_totalMahasiswa(1)

--Edit FUNCTION
ALTER FUNCTION FN_totalMahasiswa(@id INT, @nama VARCHAR (50))
RETURNS INT
BEGIN
	DECLARE @hasil INT
	SELECT @hasil = COUNT(id) FROM mahasiswa 
	WHERE id = @id AND name = @nama
	RETURN @hasil
END

SELECT dbo.FN_totalMahasiswa(2, 'Imam') AS jumlahData

-- select function mahasiswa
SELECT 
	mahasiswa.id, 
	mahasiswa.name, 
	dbo.FN_totalMahasiswa(mahasiswa.id,mahasiswa.name)
FROM mahasiswa
JOIN biodata ON biodata.mahasiswa_id = mahasiswa.id

-- mereset table / Delete dan reset isi table
TRUNCATE TABLE [nama_table]