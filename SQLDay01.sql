-- SQLDAY01

-- DDL SEDERHANA

-- CREATE

-- Membuat Database
CREATE DATABASE db_kampus

-- Pindah ke database menggunakan Query
USE db_kampus

-- Drop DataBase
/* DROP db_Kampus */

-- Membuat Table
CREATE TABLE mahasiswa(
	id BIGINT PRIMARY KEY IDENTITY(1,1),
	name VARCHAR(50) NOT NULL,
	address VARCHAR(50) NOT NULL,
	email VARCHAR(225) NULL
)

-- Membuat VIEW
CREATE VIEW vw_mahasiswa 
AS SELECT * FROM mahasiswa

/* --------------------------------------------------------------------------------*/

-- ALTER
-- Menambah Column
ALTER TABLE mahasiswa ADD nomor_hp VARCHAR(15) NOT NULL

-- drop column
ALTER TABLE mahasiswa DROP COLUMN nomor_hp

-- mengubah column
ALTER TABLE mahasiswa ALTER COLUMN email VARCHAR(100) NOT NULL

/* --------------------------------------------------------------------------------*/

--DROP
-- drop database
DROP DATABASE [nama_database]

-- drop table
DROP TABLE [nama_table]

-- drop view
drop view [nama_view]

-- drop column
ALTER TABLE [nama_table] DROP COLUMN [nama_column]

/* --------------------------------------------------------------------------------*/

-- DML

-- Insert
INSERT INTO 
	mahasiswa(name, address, email) 
VALUES 
('Ali','Kemayoran','aliirvan122@gmail.com')
	--('Imam','Bekasi','imamassidqi@gmail.com'),
	--('haikal','Kuningan','haikal5nsah@gmail.com'),
	--('rezki','Cengkareng','rezkyfajri@gmail.com'),
	--('tunggul','Semarang','tunggulyudhaputra@gmail.com'),
	--('Shabrina','Palembang','shabrinaputrif1604@gmail.com'),
	--('TEST','Palembang','shabrinaputrif1604@gmail.com')
	

-- SELECT
SELECT TOP 3
	id, name, address, email
FROM
	mahasiswa

-- UPDATE
UPDATE mahasiswa SET name = 'Haikal Limansyah' WHERE id = 3

-- DELETE
DELETE mahasiswa WHERE name = 'TEST'

/*======================================================================*/

-- membuat tabel biodata
CREATE TABLE biodata(
	id BIGINT PRIMARY KEY IDENTITY(1,1),
	mahasiswa_id BIGINT NULL,
	tgl_lahir DATE NULL,
	gender VARCHAR(10) NULL
)

SELECT * FROM biodata

-- masukkan data
INSERT INTO biodata(mahasiswa_id, tgl_lahir, gender)
VALUES
	(1, '2020-06-10', 'Pria'),
	(2, '2021-07-10', 'Pria'),
	(3, '2022-08-10', 'Wanita')

-- JOIN ( AND, NOT, OR) dan ORDER BY
SELECT
	mhs.id,
	mhs.name AS nama,
	mhs.address AS alamat,
	mhs.email AS email,
	bio.tgl_lahir AS Tanggal_Lahir,
	bio.gender
FROM
	mahasiswa AS mhs
JOIN
	biodata AS bio
ON
	mhs.id = bio.mahasiswa_id
ORDER BY
	bio.tgl_lahir DESC, bio.gender ASC

-- TOP
SELECT TOP 2
	mhs.id,
	mhs.name AS nama,
	mhs.address AS alamat,
	mhs.email AS email,
	bio.tgl_lahir AS Tanggal_Lahir,
	bio.gender
FROM
	mahasiswa AS mhs
JOIN
	biodata AS bio
ON
	mhs.id = bio.mahasiswa_id
ORDER BY
	bio.tgl_lahir DESC, bio.gender ASC

-- BETWEEN
SELECT
	mhs.id,
	mhs.name AS nama,
	mhs.address AS alamat,
	mhs.email AS email,
	bio.tgl_lahir AS Tanggal_Lahir,
	bio.gender
FROM
	mahasiswa AS mhs
JOIN
	biodata AS bio
ON
	mhs.id = bio.mahasiswa_id
WHERE
	mhs.id BETWEEN 1 and 2
ORDER BY
	bio.tgl_lahir DESC, bio.gender ASC

-- LIKE
select * FROM mahasiswa
WHERE
	name LIKE 'i%' -- awalan i
	/*
	name LIKE '%a' -- akhiran a
	name LIKE '%ez%' -- mengandung ez di semua posisi
	name LIKE '_a%' -- karakter kedua adalah a
	name LIKE 'a__%' -- karakter pertama a, minimal 3 huruf
	name LIKE 'a%0' -- awalan a akhiran o
	*/

-- group by
SELECT name FROM mahasiswa
GROUP BY
	name

-- aggregate function (MIN, MAX, COUNT, SUM)
SELECT 
	SUM(id), name, address
FROM
	mahasiswa
GROUP BY
	name, address

UPDATE mahasiswa SET address = 'JOGJA' WHERE id = 8

-- Having 
SELECT
	SUM(id) AS jumlah, 
	name
FROM
	mahasiswa
GROUP BY
	name
HAVING
	SUM(id) > 4
ORDER BY
	jumlah ASC

