-- TUGASSQLDay01

CREATE DATABASE DBPenerbit

-- Buatlah Tabel Pengarang
CREATE TABLE tblPengarang(
	ID BIGINT PRIMARY KEY IDENTITY(1,1),
	Kd_Pengarang VARCHAR(7) NOT NULL,
	Nama VARCHAR(30) NOT NULL,
	Alamat VARCHAR(80) NOT NULL,
	Kota VARCHAR(15) NOT NULL,
	Kelamin VARCHAR(1) NOT NULL
)

-- Masukkan data kedalam tabel
INSERT INTO tblPengarang(Kd_Pengarang, Nama, Alamat, Kota, Kelamin)
VALUES
	('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
	('P0002','Rian','Jl. Solo 123','Yogya','P'),
	('P0003','Suwadi','Jl. Semangka 13','Bandung','P'),
	('P0004','Siti','Jl. Durian 15','Solo','W'),
	('P0005','Amir','Jl. Gajah 33','Kudus','P'),
	('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
	('P0007','Jaja','Jl. Singa 7','Bandung','P'),
	('P0008','Saman','Jl. Naga 12','Yogya','P'),
	('P0009','Anwar','Jl. TIdar 6A','Magelang','P'),
	('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')

-- Buatlah tabel gaji
CREATE TABLE tblGaji(
	ID BIGINT PRIMARY KEY IDENTITY(1,1),
	Kd_Pengarang VARCHAR(7) NOT NULL,
	Nama VARCHAR(30) NOT NULL,
	Gaji DECIMAL(18,4) NOT NULL
)

-- Masukkan data dalam tabel gaji
INSERT INTO tblGaji(Kd_Pengarang, Nama, Gaji)
VALUES
	('P0002','Rian',600000),
	('P0005','Amir',700000),
	('P0004','Siti',500000),
	('P0003','Suwadi',1000000),
	('P0010','Fatmawati',600000),
	('P0008','Saman',750000)

-- 1. Hitung dan tampilkan jumlah pengarang dari table tblPengarang
SELECT
	COUNT(tblPengarang.ID)
FROM
	tblPengarang

-- 2. Hitung berapa jumlah pengarang pria dan wanita
SELECT
	kelamin, COUNT(tblPengarang.Kelamin) AS Jumlah
FROM
	tblPengarang
GROUP BY 
	kelamin

-- 3. Tampilkan record kota dan jumlah kotanya dari tblPengarang
SELECT
	tblPengarang.Kota,
	COUNT(tblPengarang.Kota) AS jumlahPengarang
FROM 
	tblPengarang
GROUP BY 
	tblPengarang.Kota
HAVING
	NOT Kota = 'Magelang' AND NOT Kota = 'Solo'
ORDER BY
	jumlahPengarang DESC, Kota ASC


-- 4. Tampilkan record kota diatas 1 kota dari tblPengarang
SELECT
	kota, 
	COUNT(kota) AS Jumlah
FROM 
	tblPengarang
GROUP BY 
	kota
HAVING 
	COUNT(kota) > 1

-- 5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari tblPengarang
SELECT 
	MAX(Kd_Pengarang) AS Terbesar, 
	MIN(Kd_Pengarang) AS Terkecil
FROM 
	tblPengarang

-- 6. Tampilkan gaji tertinggi dan terendah
SELECT
	MAX(gaji) AS TERTINGGI, 
	MIN(gaji) AS TERENDAH
FROM 
	tblGaji

-- 7. Tampilkan Gaji diatas 600.000
SELECT 
	gaji
FROM 
	tblGaji
WHERE 
	gaji > 600000

-- 8. Tampilkan Jumlah Gaji
SELECT 
	SUM(gaji)
FROM 
	tblGaji

-- 9. Tampilkan jumlah gaji berdasarkan kota
SELECT
	tblPengarang.kota,
	SUM(tblGaji.Gaji) AS jumlahGaji
FROM
	tblPengarang
INNER JOIN
	tblGaji
ON
	tblPengarang.Nama = tblGaji.Nama
GROUP BY
	tblPengarang.Kota

-- 10. Tampilkan seluruh record pengarang antara P0003 - P0006 dari tabel pengarang
SELECT *
FROM
	tblPengarang
WHERE
	Kd_Pengarang BETWEEN 'P0003' AND 'P0006'

-- 11. Tampilkan seruh data Yogya, Solo, dan Magelang dari Table Pengarang
SELECT *
FROM
	tblPengarang
WHERE
	Kota = 'Yogya' OR Kota = 'Solo' OR Kota = 'Magelang'

SELECT *
FROM
	tblPengarang
WHERE
	Kota IN ('Yogya','Solo','Magelang')

-- 12. Tampilkan seluruh data bukan Yogya dari tabel Pengarang
SELECT *
FROM
	tblPengarang
WHERE
	NOT Kota = 'Yogya'

-- 13. Tampilkan seluruh data pengarang yang nama :
-- a. dimulai dengan huruf 'a'
SELECT *
FROM 
	tblPengarang
WHERE
	Nama LIKE 'a%'
-- b. berakhiran 'i'
SELECT *
FROM
	tblPengarang
WHERE
	Nama LIKE '%i'
-- c. huruf ketiganya 'a'
SELECT *
FROM
	tblPengarang
WHERE
	Nama LIKE '__a%'
-- d. tidak berakhiran 'n'
SELECT *
FROM
	tblPengarang
WHERE 
	Nama NOT LIKE '%n'

-- 14. Tampilkan seluruh data tblPengarang dan tblGaji dengan Kd_Pengarang yang sama
SELECT 
	*
FROM
	tblPengarang
JOIN
	tblGaji
ON
	tblPengarang.Kd_Pengarang = tblGaji.Kd_Pengarang

--15. Tampilkan Kota dengan gaji dibawah 100.000
SELECT
	tblPengarang.Kota,
	tblGaji.Gaji
FROM
	tblPengarang
JOIN
	tblGaji
ON
	tblPengarang.Kd_Pengarang = tblGaji.Kd_Pengarang
WHERE
	Gaji < 1000000

-- 16. Ubah panjang tipe kelaming menjadi 10
ALTER TABLE tblPengarang ALTER COLUMN Kelamin VARCHAR(10) NOT NULL

-- 17. Tambahkan Kolom [Gelar] dengan tipe VARCHAR(12) pada tblPengarang
ALTER TABLE tblPengarang ADD Gelar VARCHAR(12) -- column baru memang NULL, karna belum ada isian
ALTER TABLE tblPengarang DROP COLUMN Gelar
ALTER TABLE tblPengarang ALTER COLUMN Gelar VARCHAR(12) NOT NULL -- setelah terisi semua baru bisa di NOT NULL kan

-- 18. Ubah alamat dan kota Rian di tblPengarang menjadi JL. Cendrawasih 65 dan Pekan Baru
UPDATE tblPengarang SET Alamat = 'Jl. Cendrawasih 65' WHERE Nama = 'Rian'
UPDATE tblPengarang SET Kota = 'Pekanbaru' WHERE Nama = 'Rian'

-- 19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota, Gaji dengan nama vwPengarang
CREATE VIEW vwPengarang AS
SELECT
	tblPengarang.Kd_Pengarang,
	tblPengarang.Nama,
	tblPengarang.Kota,
	tblGaji.Gaji
FROM
	tblPengarang
LEFT JOIN
	tblGaji
ON
	tblPengarang.Kd_Pengarang = tblGaji.Kd_Pengarang;

SELECT *
FROM vwPengarang
