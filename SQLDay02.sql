-- ADD COLUMN pada tabel mahasiswa
ALTER TABLE mahasiswa ADD nilai INT

SELECT * FROM mahasiswa

-- Menambhakan nilai column baru
UPDATE mahasiswa SET nilai =  80 WHERE id = 1
UPDATE mahasiswa SET nilai =  80 WHERE id = 2
UPDATE mahasiswa SET nilai =  70 WHERE id = 3
UPDATE mahasiswa SET nilai =  100 WHERE id = 4
UPDATE mahasiswa SET nilai =  70 WHERE id = 5
UPDATE mahasiswa SET nilai =  50 WHERE id = 6
UPDATE mahasiswa SET nilai =  40 WHERE id = 8

-- AVG SUM
SELECT AVG(nilai) 
FROM mahasiswa

SELECT 
	SUM(nilai),
	name
FROM
	mahasiswa
GROUP BY 
	name


SELECT
	*
FROM
	mahasiswa
INNER JOIN
	biodata
ON
	mahasiswa.id = biodata.mahasiswa_id

-- LEFT JOIN
SELECT
	*
FROM
	mahasiswa
LEFT JOIN
	biodata
ON
	mahasiswa.id = biodata.mahasiswa_id
WHERE
	biodata.mahasiswa_id IS NULL

-- RIGHT JOIN
SELECT
	*
FROM
	mahasiswa
RIGHT JOIN
	biodata
ON
	mahasiswa.id = biodata.mahasiswa_id

-- DISTINCT
SELECT * FROM mahasiswa

SELECT
	DISTINCT (name) 
FROM
	mahasiswa


-- SUBSTRING
SELECT SUBSTRING('SQL TUTORIAL',1,3)

--Char Index
SELECT CHARINDEX('T','Customer') -- RETURN 0 if not found

-- DATA LENGTH
SELECT DATALENGTH('ALI IRVAN')

-- CASE WHEN
SELECT
	name,
	nilai,
	CASE
		WHEN nilai >= 80 THEN 'A'
		WHEN nilai >= 60 THEN 'B'
		ELSE 'C'
	END
		AS grade
FROM 
	mahasiswa

--CONCAT
SELECT CONCAT('SQL ','is ','FUN')
SELECT CONCAT('Nama = ',name) FROM mahasiswa

CREATE TABLE penjualan(
	id BIGINT PRIMARY KEY IDENTITY(1,1),
	nama VARCHAR(50) NOT NULL,
	harga DECIMAL(18,0) NOT NULL,
)

INSERT INTO	
	penjualan (nama, harga)
VALUES
	('Indomie',1500),
	('Close Up',3500),
	('Pepsodent',3000),
	('Brush Formula',2500),
	('Roti Manis',1000),
	('Gula',3500),
	('Sarden',4500),
	('Sampoerna',11000),
	('234',11000)

SELECT nama, harga, harga*100 AS [harga * 100]
FROM penjualan


-- GETDATE
SELECT GETDATE()
SELECT DAY(GETDATE()) AS DAY
SELECT MONTH('12-08-1991')

-- DATEADD
SELECT DATEADD(YEAR,5,GETDATE())

-- DATE DIFF
SELECT DATEDIFF(YEAR,'12-08-1997',GETDATE())
SELECT DATEDIFF(DAY,'2023-11-10',DATEADD(day,5,GETDATE()))


--SUBQUERY
SELECT 
	*
FROM
	mahasiswa
LEFT JOIN 
	biodata 
ON
	mahasiswa.id = biodata.mahasiswa_id
WHERE
	mahasiswa.nilai = (SELECT MAX(nilai) FROM mahasiswa)

CREATE TABLE mahasiswanew (
	id BIGINT PRIMARY KEY IDENTITY(1,1),
	name VARCHAR(50) NOT NULL,
	address VARCHAR(50) NOT NULL,
	email VARCHAR(225) NULL
)

INSERT INTO mahasiswanew (name, address, email)
SELECT
	name, address, email
FROM 
	mahasiswa

-- alter view


-- INDEX
CREATE INDEX index_nameemail 
ON mahasiswa(name, email)

CREATE UNIQUE INDEX index_id 
ON mahasiswa(id)

DROP INDEX index_id
ON mahasiswa

--PRIMARY KEY
CREATE TABLE coba(
	id INT NOT NULL,
	nama VARCHAR(50) NOT NULL
)

ALTER TABLE coba
ADD CONSTRAINT PK_id PRIMARY KEY(id)

ALTER TABLE coba
ADD CONSTRAINT PK_idnama PRIMARY KEY (id, nama) -- Primary key dapat lebih dari 1, namun harus di buat dari awal, tidak bisa ditambah di tengah jalan

drop table coba
-- hapus data
DELETE coba

-- drop primary key
ALTER TABLE coba DROP CONSTRAINT pk_idnama 

-- unique key
ALTER TABLE coba ADD CONSTRAINT unique_nama UNIQUE(nama)

-- foreign key
ALTER TABLE biodata ADD CONSTRAINT fk_mahasiswa_id FOREIGN KEY (mahasiswa_id) REFERENCES mahasiswa(id)

-- drop foreign key
ALTER TABLE biodata DROP CONSTRAINT fk_mahasiswa_id 