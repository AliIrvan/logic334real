-- Membuat Database
CREATE DATABASE DB_HR

CREATE TABLE tb_karyawan(
	id BIGINT PRIMARY KEY IDENTITY(1,1),
	nip VARCHAR(50) NOT NULL,
	nama_depan VARCHAR(50) NOT NULL,
	nama_belakang VARCHAR(50) NOT NULL,
	jenis_kelamin VARCHAR(50) NOT NULL,
	agama VARCHAR(50) NOT NULL,
	tempat_lahir VARCHAR(50) NOT NULL,
	tgl_lahir DATE,
	alamat VARCHAR(100) NOT NULL,
	pendidikan_terakhir VARCHAR(50) NOT NULL,
	tgl_masuk DATE,
)

CREATE TABLE tb_divisi(
	id BIGINT PRIMARY KEY IDENTITY(1,1),
	kd_divisi VARCHAR(50) NOT NULL,
	nama_divisi VARCHAR(50) NOT NULL
)

CREATE TABLE tb_jabatan(
	id BIGINT PRIMARY KEY IDENTITY(1,1),
	kd_jabatan VARCHAR(50) NOT NULL,
	nama_jabatan VARCHAR(50) NOT NULL,
	gaji_pokok NUMERIC,
	tunjangan_jabatan NUMERIC
)

CREATE TABLE tb_pekerjaan(
	id BIGINT PRIMARY KEY IDENTITY(1,1),
	nip VARCHAR(50) NOT NULL,
	kode_jabatan VARCHAR(50) NOT NULL,
	kode_divisi VARCHAR(50) NOT NULL,
	tunjangan_kinerja NUMERIC,
	kota_penempatan VARCHAR(50)
)

--Mengisi TABLE
INSERT INTO tb_karyawan(nip, nama_depan, nama_belakang, jenis_kelamin, agama, tempat_lahir, tgl_lahir, alamat, pendidikan_terakhir, tgl_masuk)
VALUES
	(001, 'Hamidi','Samsudin', 'Pria', 'Islam', 'Sukabumi', '1977-04-21', 'Jl. Sudirman No. 12', 'S1 Teknik Mesin', '2015-12-07'),
	(002, 'Ghandi','Wamida', 'Wanita', 'Islam', 'Palu', '1992-01-12', 'Jl. Rambutan No. 22', 'SMA Negeri 02 Palu', '2014-12-01'),
 	(003, 'Paul','Christian', 'Pria', 'Kristen', 'Ambon', '1980-05-27', 'Jl. Veteran No. 4', 'S1 Pendidikan Geografi', '2014-01-12')

INSERT INTO tb_divisi(kd_divisi, nama_divisi)
VALUES
	('GD', 'Gudang'),
	('HRD', 'HRD'),
	('KU', 'Keuangan'),
	('UM', 'Umum')

INSERT INTO tb_jabatan(kd_jabatan, nama_jabatan, gaji_pokok, tunjangan_jabatan)
VALUES
	('MGR', 'Manager', 5500000, 1500000),
	('OB', 'Office Boy', 1900000, 200000),
	('ST', 'Staff', 3000000, 750000),
	('WMGR', 'Wakil Manager', 4000000, 1200000)

INSERT INTO tb_pekerjaan(nip, kode_jabatan, kode_divisi, tunjangan_kinerja, kota_penempatan)
VALUES
	('001', 'ST', 'KU', 750000, 'Cianjur'),
	('002', 'OB', 'UM', 350000, 'Sukabumi'),
	('003', 'MGR', 'HRD', 1500000, 'Sukabumi')




-- 1. Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji, yang gaji + tunjangan kinerja dibawah 5 juta
SELECT
	CONCAT(kry.nama_depan, ' ' , kry.nama_belakang) AS namaLengkap,
	jbt.nama_jabatan,
	(jbt.gaji_pokok + jbt.tunjangan_jabatan) AS totalGaji
FROM
	tb_karyawan AS kry
LEFT JOIN
	tb_pekerjaan AS pkj
ON
	kry.nip = pkj.nip
LEFT JOIN
	tb_jabatan AS jbt
ON
	pkj.kode_jabatan = jbt.kd_jabatan
WHERE 
	(jbt.gaji_pokok + pkj.tunjangan_kinerja) < 5000000

-- 2. Tampilkan nama Lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yang gender pria, penempatan di luar suka bumi
SELECT
	CONCAT(tb_karyawan.nama_depan, ' ' , tb_karyawan.nama_belakang) AS namaLengkap,
	tb_jabatan.nama_jabatan,
	tb_divisi.nama_divisi,
	(tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) AS totalGaji,
	CAST(((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 5 / 100) AS NUMERIC(18,2))  AS pajak,
	CAST(((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) - ((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 5 / 100)) AS NUMERIC(18,2)) AS gajiBersih
FROM
	tb_karyawan
LEFT JOIN
	tb_pekerjaan
ON
	tb_karyawan.nip = tb_pekerjaan.nip
INNER JOIN
	tb_jabatan
ON
	tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
INNER JOIN
	tb_divisi
ON
	tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
WHERE
	NOT tb_pekerjaan.kota_penempatan = 'Sukabumi' 

-- 3. Tampilkan nama lengkap, jabatan, nama divisi, bonus * 7
SELECT
	tb_karyawan.nip,
	CONCAT(tb_karyawan.nama_depan, ' ' , tb_karyawan.nama_belakang) AS namaLengkap, --COONCAT_WS(' ',nama_depan, nama_belakang)
	tb_jabatan.nama_jabatan,
	tb_divisi.nama_divisi,
	CAST((((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 25 /100) * 7) AS NUMERIC(18,2)) AS bonus
FROM
	tb_karyawan
LEFT JOIN
	tb_pekerjaan
ON
	tb_karyawan.nip = tb_pekerjaan.nip
INNER JOIN
	tb_jabatan
ON
	tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
INNER JOIN
	tb_divisi
ON
	tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi

-- 4. tampilkan nama lengkap, total gaji, infak yang mempunyai jabatan MGR
SELECT
	tb_karyawan.nip,
	CONCAT(tb_karyawan.nama_depan, ' ' , tb_karyawan.nama_belakang) AS namaLengkap,
	tb_jabatan.nama_jabatan,
	tb_divisi.nama_divisi,
	CAST((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) AS NUMERIC(18,2)) AS totalgaji,
	CAST(((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 5 / 100 ) AS NUMERIC(18,2)) AS infak
FROM
	tb_karyawan
LEFT JOIN
	tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
INNER JOIN
	tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
INNER JOIN
	tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
WHERE
	tb_jabatan.kd_jabatan = 'MGR'

-- 5. Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan (2 juta), total gaji (gapok + tunj.jabatan + tunj.Pendidikan) diaman pendidikan terakhir adalah S1
SELECT
	tb_karyawan.nip,
	CONCAT(tb_karyawan.nama_depan, ' ' , tb_karyawan.nama_belakang) AS namaLengkap,
	tb_jabatan.nama_jabatan,
	tb_karyawan.pendidikan_terakhir,
	(2000000) AS tunjanganPendidikan,
	(tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + 2000000) AS totalGaji
FROM
	tb_karyawan
LEFT JOIN
	tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
INNER JOIN
	tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
WHERE
	tb_karyawan.pendidikan_terakhir LIKE 'S1%'
ORDER BY 
	totalGaji ASC

-- 6. Tampilkan NIP, nama Lengkap, jabatan, nama divisi, bonus
SELECT
	tb_karyawan.nip,
	CONCAT(tb_karyawan.nama_depan, ' ' , tb_karyawan.nama_belakang) AS namaLengkap,
	tb_jabatan.nama_jabatan,
	tb_divisi.nama_divisi,
	CASE
		WHEN tb_jabatan.kd_jabatan = 'MGR' THEN CAST((((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 7) * 25 / 100)  AS NUMERIC(18,2))
		WHEN tb_jabatan.kd_jabatan = 'ST' THEN CAST((((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 5) * 25 / 100)  AS NUMERIC(18,2))
		ELSE  CAST((((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 2) * 25 / 100)  AS NUMERIC(18,2))
	END AS bonus
FROM
	tb_karyawan
LEFT JOIN
	tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
INNER JOIN
	tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
INNER JOIN
	tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi

-- 7. buatlah kolom NIP pada tabel karyawan menjadi kolom unique
ALTER TABLE tb_karyawan ADD CONSTRAINT unique_nip UNIQUE(nip)

-- 8. buatlah kolom NIP pada tabel karyawan sebagai index
CREATE INDEX index_nipKaryawan
ON tb_karyawan(nip)

-- 9. Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf kapital dengan kondisi nama belakang diawali dengan huruf W
SELECT
	CONCAT(tb_karyawan.nama_depan,' ', tb_karyawan.nama_belakang) AS namaAwal,
	CASE
		WHEN CHARINDEX('W', tb_karyawan.nama_belakang) = 1 THEN  CONCAT(tb_karyawan.nama_depan,' ', UPPER(tb_karyawan.nama_belakang))
		ELSE CONCAT(tb_karyawan.nama_depan,' ', tb_karyawan.nama_belakang)
	END AS perubahanNama
FROM
	tb_karyawan

-- 10. Tampilkan NIP, nama lengkap, jabatan, nama divisi, total gaji, bonus, lama bekerja (bonus 10% untuk karyawan lebih sama dengan 8 tahun)
SELECT
	tb_karyawan.nip,
	CONCAT_WS( ' ' ,tb_karyawan.nama_depan, tb_karyawan.nama_belakang) AS namaLengkap,
	tb_karyawan.tgl_masuk,
	tb_jabatan.nama_jabatan,
	tb_divisi.nama_divisi,
	CAST((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) AS NUMERIC(18,1)) AS totalGaji,
	CAST(((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 10 / 100) AS NUMERIC(18,1)) AS bonus,
	FLOOR(DATEDIFF(DAY, tb_karyawan.tgl_masuk, GETDATE()) / 365.25) AS lamaBekerja,
	CONVERT(NUMERIC(18,1), (tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja)) AS totalGaji2
	---SELECT ROUND(6125.4349, 2)
FROM
	tb_karyawan
INNER JOIN
	tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
INNER JOIN
	tb_jabatan ON  tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
INNER JOIN
	tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
WHERE
	FLOOR(DATEDIFF(DAY, tb_karyawan.tgl_masuk, GETDATE()) / 365.25) >= 8
